import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';
import { Grid, AppBar, Toolbar, Typography, Paper, FormControl, Input, Icon, withStyles } from '@material-ui/core';
import { BorderColor, Dvr } from '@material-ui/icons';
import Marked from 'marked';

const styles = theme => ({
  root: {
    padding: '0 8px'
  },
  appBar: {
    backgroundColor: '#263238',
    color: '#ffffff'
  },
  paper: {
    ...theme.mixins.gutters(),
    marginTop: '8px',
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    backgroundColor: '#455A64',
    color: '#ffffff'
  },
  textField: {
    color: '#ffffff'
  },
  icon: {
    marginRight: theme.spacing.unit
  }
});

// allows line breaks with return button
Marked.setOptions({
  breaks: true,
});

// inserts target="_blank" into href tags (required for codepen links)
const renderer = new Marked.Renderer();
renderer.link = function (href, title, text) {
  return `<a target="_blank" href="${href}">${text}</a>`;
}

const placeholder =
`
  # Welcome to my React Markdown Previewer!
  
  ## This is a sub-heading...
  ### And here's some other cool stuff:
    
  Heres some code, \`<div></div>\`, between 2 backticks.
  
  \`\`\`
  // this is multi-line code:
  
  function anotherExample(firstLine, lastLine) {
    if (firstLine == '\`\`\`' && lastLine == '\`\`\`') {
      return multiLineCode;
    }
  }
  \`\`\`
    
  You can also make text **bold**... whoa!
  Or _italic_.
  Or... wait for it... **_both!_**
  And feel free to go crazy ~~crossing stuff out~~.
  
  There's also [links](https://www.freecodecamp.com), and
  > Block Quotes!
  
  And if you want to get really crazy, even tables:
  
  Wild Header | Crazy Header | Another Header?
  ------------ | ------------- | ------------- 
  Your content can | be here, and it | can be here....
  And here. | Okay. | I think we get it.
  
  - And of course there are lists.
    - Some are bulleted.
        - With different indentation levels.
          - That look like this.
  
  
  1. And there are numbererd lists too.
  1. Use just 1s if you want! 
  1. But the list goes on...
  - Even if you use dashes or asterisks.
  * And last but not least, let's not forget embedded images:
  
  ![React Logo w/ Text](https://goo.gl/Umyytc)`;

class App extends Component {
  state = {
    editor: placeholder
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid container spacing={8}>
          <Grid item xs={12} sm={6}>
            <AppBar position="static" className={classes.appBar}>
              <Toolbar>
                <Icon color="inherit" className={classes.icon} aria-hidden={true}>
                  <BorderColor />
                </Icon>
                <Typography color="inherit" variant="h6">
                  Editor
                </Typography>
              </Toolbar>
            </AppBar>
            <Paper className={classes.paper} elevation={1}>
              <FormControl className={classes.formControl} margin="normal" fullWidth>
                <Input id="editor" multiline value={this.state.editor} onChange={this._updatePreview} className={classes.textField}  />
              </FormControl>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <AppBar position="static" className={classes.appBar}>
              <Toolbar>
                <Icon color="inherit" className={classes.icon} aria-hidden={true}>
                  <Dvr />
                </Icon>
                <Typography color="inherit" variant="h6">
                  Preview
                </Typography>
              </Toolbar>
            </AppBar>
            <Paper className={classes.paper} elevation={1}>
              <span id="preview" dangerouslySetInnerHTML={{ __html: Marked(this.state.editor, { renderer: renderer }) }}></span>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }

  _updatePreview = event => {
    this.setState({ editor: event.target.value });
  }
};

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);